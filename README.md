# Exo Jeu

## Consigne
### I. Faire en sorte d'avoir un "personnage"/"vaisseau"/"truc" qu'on puisse déplacer
1. Dans le HTML, mettre une div avec une class character, et dans le css faire que ça soit un carré rouge en absolute de 50 par 50px par exemple (à terme, vous pouvez lui mettre une background-image pour lui donner l'apparence que vous voulez)
2. Toujours dans le HTML, mettre une div.playzone et dans le CSS, faire que ça soit un rectangle avec une bordure, de 100vh de hauteur et 600px de largeur par exemple, et le centrer (avec des margin auto), et le mettre en position relative
3. Côté JS, capturer le character ainsi que le playzone, chacun dans sa variable
4. Toujours dans le JS, rajouter un eventListener sur la playzone pour l'event mousemove (qui se déclenche quand la souris bouge, comme son nom l'indique), et commencer par juste vérifier si ça marche avec un ptit console.log
5. A l'intérieur de l'eventlistener, faire en sorte de récupérer la position x et y de la souris (vous pouvez chercher sur internet, sinon je vous donne un indice en dessous)
indice: Pour récupérer cette position, il faut utiliser la variable event de la fonction de l'eventListener, et choper le offsetX et offsetY sur cette variable event
Afficher ces positions avec des console.log pour voir si ça marche bien
6. Faire en sorte d'assigner la position x de la souris au style left du character, et la position y à son style top (pour ça, il faudra lui rajouter +'px' à la fin)
Et là normalement, on a un carré qui bouge

Bonus (qui en fait annule un peu toutes les étapes d'avant et est plus compliqué, donc c'est pour les très motivées, ou pour plus tard) : Faire en sorte que le character se déplace avec les flêches du clavier (genre si j'appuie sur la touche gauche, on lui enlève 10 à son left)
### II. Rajouter des obstacle qui nous font mal
1. Dans le HTML, rajouter 2 nouvelles div à l'intérieur de la playzone avec une class obstacle. Dans le css, faire que les .obstacle soient des carrés de 20x20px par exemple, avec une background-color (ou alors une background-image), et toujours en absolute
2. Dans le JS, capturer les .obstacle avec un querySelectorAll (celui ci va nous renvoyer un tableau d'élément, il va donc falloir faire une boucle pour pouvoir l'exploiter, on va donc commencer par ça)
3. Faire un for...of sur les obstacles, et à l'intérieur de cette boucle, on va assigner une position random à chaque obstacle, pour ce faire, assigner au style left une valeur entre 0 et 600 et au style top une valeur entre 0 et 300, je vous donne en dessous la fonction qui permet de choper un nombre random entre 2 valeurs
4. Dans le addEventListener mousemove fait plus tôt, rajouter une boucle for..of sur les obstacles et à l'intérieur de cette boucle faire en sorte de faire un console log s'il y a collision entre un des obstacles et le character (fonction de collision plus basse)
5. On peut maintenant multiplier les div.obstacles dans le html pour augmenter la difficulté, et mettre autre chose plutôt qu'un console.log si jamais ya collision (genre supprimer le vaisseau)


## Ressources

### Fonction de random entre deux valeurs
```js
function randomRange(min,max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
```

### Fonction de détection de collision entre deux éléments html
```js
function isColliding(element1, element2) {
    let pos1 = element1.getBoundingClientRect();
    let pos2 = element2.getBoundingClientRect();
    if (pos1.x < pos2.x + pos2.width &&
        pos1.x + pos1.width > pos2.x &&
        pos1.y < pos2.y + pos2.height &&
        pos1.height + pos1.y > pos2.y) {
         return true;
     }
     return false;
}
```