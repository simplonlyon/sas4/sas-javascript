/**
 * Perso j'aime bien regrouper mes différentes captures d'éléments/créations de variable
 * Ici on récupère le bouton, le paragraphe et le select
 */
let button = document.querySelector('button');
let monPara = document.querySelector('#mon-para');
let selectColor = document.querySelector('#select-color');


// On ajoute un event au click sur le bouton qui fait que quand on click...
button.addEventListener('click', function () {
    //...ça vient modifier le texte du paragraphe par autre chose
    monPara.textContent = 'Bouton cliqué';

});

//On ajoute un autre event au double click sur le paragraphe lui même...
monPara.addEventListener('dblclick', function() {
    //...qui vient augmenter sa taille lorsqu'on double click dessus
    monPara.style.fontSize = '2em';
});

//Enfin, on ajoute un event sur le select qui se déclenchera au moment où la valeur
//du select est modifiée...
selectColor.addEventListener('change', function() {
    //...on récupère la valeur actuelle du select avec le .value et on assigne
    //celle ci à la couleur du texte du paragraphe
    monPara.style.color = selectColor.value;
});

/**
 * 1. Dans le HTML rajouter un paragraphe avec un id "mon-para" *
 * 2. Dans le JS capturer ce paragraphe *
 * 3. Dans le eventListener du button, faire en sorte de changer
 * le texte de #mon-para par "bouton cliqué" *
 * 4. Toujours dans le JS, rajouter un eventListener au dblclick
 * sur #mon-para et faire en sorte que quand on double click dessus
 * ça change la taille du texte pour la mettre à 2em
 * 5. Dans le HTML, rajouter une balise select avec un id "select-color" qui aura
 * plusieurs balises option dedans avec "red", "green", "blue" en value
 * 6. Dans le JS, capturer le select et ajouter un eventListener dessus pour l'event
 * "change"
 * 7. Dans cet eventListener, faire en sorte d'afficher en console la value du du select
 * (même principe que si on voulait afficher son textContent, mais là c'est sa value)
 * 8. Toujours dans l'eventListener, faire en sorte de modifier la couleur du texte de
 * #mon-para et lui assigner la value du select (celle qu'on a console log avant)
 */