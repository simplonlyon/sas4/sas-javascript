//On crée une variable qui va contenir la valeur 'Rama'
let prenom = 'Rama';

prenom = 'Jean';

let nombre = 10.5;
let booleenne = true; //ou false


console.log(prenom);

//On récupère dans le document HTML l'élément qui a un id "para"
let elementParagraphe = document.querySelector('#para');

//On modifie le contenu textuel de l'élément en question
elementParagraphe.textContent = prenom;

//On modifie le style css de l'élément pour mettre sa couleur de texte en rouge
elementParagraphe.style.color = 'red';
elementParagraphe.style.backgroundColor = 'blue';
elementParagraphe.style.fontWeight = 'bold';


//modifier le paragraphe pour le mettre en rouge
// avec un fond en bleu
// et en gras
// elementParagraphe.style.